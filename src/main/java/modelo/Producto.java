package modelo;

/**
 *
 * @author omaro
 */
public class Producto {
    private String id;
    private String nombre;
    private float temperatura;
    private float valor;
    
    public Producto() {}

    
    public Producto(String id,String nombre,float temperatura,float valor){
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valor = valor;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
    
    public float calcularCosto(){
        
        System.out.println(this.temperatura); 
        System.out.println(this.valor);  
        
        if(this.temperatura >= 0 && this.temperatura<=20){
            return this.valor = this.valor+(this.valor*20/100);
        }else{
            return this.valor = this.valor+(this.valor*10/100);
        }
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valor=" + valor + '}';
    }
}
