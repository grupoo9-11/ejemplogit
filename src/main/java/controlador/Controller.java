
package controlador;


import modelo.CRUD;
import modelo.Producto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Persistencia.DbConnection;

public abstract class Controller {
    
    public static boolean registrarProductos(String id,String nombre,float temperatura,float valor){
        
        Producto p1 = new Producto(id, nombre, temperatura, valor);
        CRUD.setConnection(DbConnection.ConexionBD());
         String sentencia = "INSERT INTO Productos(id,nombre,temperatura,valor) "
                + " VALUES ('" + p1.getId()+ "','" + p1.getNombre() + "','" + p1.getTemperatura() + "','" + p1.getValor() + "');";
         
        if (CRUD.setAutoCommitBD(false)) {
            if (CRUD.insertarBD(sentencia)) {
                CRUD.commitBD();
                CRUD.cerrarConexion();
                return true;
            } else {
                CRUD.rollbackBD();
                CRUD.cerrarConexion();
                return false;
            }
        } else {
            CRUD.cerrarConexion();
            return false;
        }
             
    }
    
    public static boolean actualizarProduco(String id,String nombre,float temperatura,float valor){
        
        
        Producto p1 = new Producto(id, nombre, temperatura, valor);
        
        CRUD.setConnection(DbConnection.ConexionBD());
        
       String sentencia = "UPDATE `productos` SET nombre='" + p1.getNombre() + "',temperatura='" + p1.getTemperatura() + "',valor='" + p1.getValor()
                + "' WHERE id=" + p1.getId() + ";";
       
        if (CRUD.setAutoCommitBD(false)) {
            if (CRUD.actualizarBD(sentencia)) {
                CRUD.commitBD();
                CRUD.cerrarConexion();
                return true;
            } else {
                CRUD.rollbackBD();
                CRUD.cerrarConexion();
                return false;
            }
        } else {
            CRUD.cerrarConexion();
            return false;
        }
    }
    
     public static boolean borrarProduco(String id){
        
        CRUD.setConnection(DbConnection.ConexionBD());
        
        String sentencia = "DELETE FROM 'productos' WHERE id=" + id + ";";
        
        if (CRUD.setAutoCommitBD(false)) {
            if (CRUD.borrarBD(sentencia)) {
                CRUD.commitBD();
                CRUD.cerrarConexion();
                return true;
            } else {
                CRUD.rollbackBD();
                CRUD.cerrarConexion();
                return false;
            }
        } else {
            CRUD.cerrarConexion();
            return false;
        }
    }
    
      public static Producto obtenerProducto(String id) {
        
        CRUD.setConnection(DbConnection.ConexionBD());
        String sentencia = "select * from productos where id=" + id + ";";
        ResultSet rs = CRUD.consultarBD(sentencia);
        Producto p1 = new Producto();
        
        try {
            if (rs.next()) {
                p1.setId(rs.getString("id"));
                p1.setNombre(rs.getString("nombre"));
                p1.setTemperatura(rs.getFloat("temperatura"));
                p1.setValor(rs.getFloat("valor"));
                CRUD.cerrarConexion();
            } else {
                CRUD.cerrarConexion();
                return null;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return p1;
    }
      
     public static List<Producto> listarProductos() {
          
        CRUD.setConnection(DbConnection.ConexionBD());
        List<Producto> listaProductos = new ArrayList<>();
        
        try {
            String sql = "select * from productos";

            ResultSet rs = CRUD.consultarBD(sql);
            
            while (rs.next()) {
                Producto p1 = new Producto();
                p1.setId(rs.getString("id"));
                p1.setNombre(rs.getString("nombre"));
                p1.setTemperatura(rs.getFloat("temperatura"));
                p1.setValor(rs.getFloat("valor"));
                listaProductos.add(p1);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        } finally {
            CRUD.cerrarConexion();
        }

        return listaProductos;
    }

}
